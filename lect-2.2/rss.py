def read_flow(name):
    import chardet
    with open(name, 'rb') as f:
        data = f.read()
        result = chardet.detect(data)
        text_source = data.decode(result['encoding'])
        return text_source


def number_of_words(text_source):
    to_list = text_source.split(' ')
    to_set = set()
    for i in to_list:
        if len(i) > 6:
            to_set.add(i)
    word_unit = {}
    for i in to_set:
        count = 0
        for j in to_list:
            if i == j:
                count += 1
        word_unit[i] = count
    return word_unit


def sort_top(word_unit):
    register = list()
    l_dict = str(len(word_unit))
    for i in word_unit.items():
        l_word = str(i[1])
        register.append((len(l_dict) - len(l_word)) * '0' + str(i[1]) + ' ' + i[0])
    register.sort(reverse=True)
    top_10_list = list()
    top_10 = {}
    count = 1
    for j in register:
        top_10[count] = j.split(' ')
        top_10[count][0] = int(top_10[count][0])
        if count == 10:
            break
        count += 1
    return top_10


def rss_news():
    while True:
        name = input(
            'Введите имя источника: / newsfr.txt / newsit.txt / newsafr.txt / newscy.txt / Для Выхода введите  "exit" : ')
        if name == 'newsfr.txt' or name == 'newsit.txt' or name == 'newsafr.txt' or name == 'newscy.txt':
            print('Читаю источник / . / . / . /')
            top_10 = sort_top(number_of_words(read_flow(name)))
            for k in top_10.values():
                print(k[0], ': ', k[1])
        elif name == 'exit':
            break
        else:
            print('Нет такого источника, введите правильный.')


rss_news()