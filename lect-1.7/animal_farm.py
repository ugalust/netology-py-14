class Farm:
    def __init__(self):
        self.farm_name = 'Ферма Петра и Сидора'
        self.location_farm = 'Воронежская область, Панинский район, село Александровка'
        self.num_animals = None


class Unit(Farm):
    def __init__(self):
        super().__init__()
        self.flies_unit = False
        self.number_of_paws = 4
        self.jump_unit = True
        self.weight_unit = None
        self.speed_unit = 2
        self.name_unit = None
        self.say_unit = 'Неопределено'
        self.horns = True

    def scream(self, say_unit=''):
        say_unit = say_unit if say_unit else self.say_unit
        print(say_unit)

    def run(self, speed=2):
        print('Ходит')
        return speed

    def stop(self):
        print('Стоит')
        self.speed_unit = 0
        return self.speed_unit


class Animal(Unit):
    def __init__(self):
        super().__init__()
        self.name = 'Крупный скот'
        self.say_unit = 'Не достаточно определения'
        self.speed_unit = 20
        self.weight_unit = 50


class Fowl(Unit):
    def __init__(self):
        super().__init__()
        self.name = 'Домашняя птица'
        self.number_of_paws = 2
        self.speed = 4
        self.flies_unit = True
        self.say_unit = 'Кря'
        self.speed_bird = 20 if self.flies_unit else 0
        self.weight = 5

    def go_fly(self, speed_bird=20):
        if self.flies_unit:
            print('Летит')
            self.speed_bird = self.speed_bird = speed_bird
        else:
            print('Я не летаю, отстань от меня.')
            self.speed_bird = speed_bird = 0
        return speed_bird

    def stop(self):
        print('Не летит')
        self.speed_unit = 0
        self.speed_bird = 0
        return self.speed_unit, self.speed_bird


class Cow(Unit):
    def __init__(self, name=''):
        super().__init__()
        self.name = name if name else 'Корова'
        self.say_unit = 'М-у-у-у...'
        self.jump = False
        self.num_animals = 4
        self.weight_unit = 150


class Goat(Unit):
    def __init__(self, name=''):
        super().__init__()
        self.name = name if name else 'Коза'
        self.say_unit = 'Б-е-е-е...'
        self.weight_unit = 60


class Sheep(Unit):
    def __init__(self, name=''):
        super().__init__()
        self.name = name if name else 'Овца'
        self.say = 'М-е-е-е...'
        self.weight = 90


class Pig(Unit):
    def __init__(self, name=''):
        super().__init__()
        self.name = name if name else 'Свинья'
        self.say_unit = 'Хрю-Хрю-Хрю...'
        self.weight = 100
        self.speed = 10


class Duck(Fowl):
    def __init__(self, name=''):
        super().__init__()
        self.name = name if name else 'Гадкий утёнок'
        self.say = 'Кря-кря-кря'
        self.weight = 10
        self.flies_unit = False


class Chicken(Fowl):
    def __init__(self, name=''):
        super().__init__()
        self.name = name if name else 'Курочка ряба'
        self.say = 'Ку-ка-ре-ку...'
        self.weight = 1, 480
        self.flies_unit = False


class Goose(Fowl):
    def __init__(self, name=''):
        super().__init__()
        self.name = name if name else 'Гуся'
        self.say = 'Га-га-га'
        self.jump = False
        self.fly = False
        self.weight = 25


cow = Cow()
print(cow.farm_name)
print(cow.location_farm)
print(f'Какое животное: {cow.name}')
print(f'Животное звучит так: {cow.say_unit}')
print(f'Вес: {cow.weight_unit}')
print(f'Прыгает: {cow.jump}')
print(cow.run())
print(cow.stop())
print(cow.run(5))
print(f'Количество {cow.name.upper()}: {cow.num_animals} штуки')

print('-' * 50)

goat = Goat()
print(goat.farm_name)
print(goat.location_farm)
print(goat.name)
print(f'Животное звучит так: {goat.say_unit}')
print(f'Вес: {goat.weight_unit}')

print('-' * 50)

sheep = Sheep()
sheep.scream()
print(sheep.name)
print(sheep.weight_unit)

print('-' * 50)

pig = Pig()
print(pig.name)
print(pig.say_unit)
print(f'Вес: {pig.weight_unit}')

print('-' * 50)

duck = Duck()
duck.scream()
print(duck.name)
print(duck.weight)
print(duck.run())
print(duck.stop())
print(duck.go_fly(15))

print('-' * 50)

chick = Chicken()
chick.scream()
print(chick.name)
print(chick.weight_unit)
print(chick.run())
print(chick.stop())
print(chick.go_fly(15))

print('-' * 50)

goose = Goose()
goose.scream()
print(goose.name)
print(goose.weight)
print(goose.jump)
print(goose.run(17))
print(goose.stop())
print(goose.go_fly(52))
